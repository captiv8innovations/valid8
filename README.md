valid8 (validate)
===================
valid8 is a validation jQuery class that checks for valid email addresses, telephone numbers and required fields.

## Terms of Use
cr8 is created and maintained by [captiv8 Innovations Inc](https://captiv8innovations.com). The code is available under the [captiv8 Terms of Use License](https://captiv8innovations.com/terms).