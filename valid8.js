if (!valid8) {
    /**
     * valid8 accepts any jQuery form object and goes through each form field and
     * validates them. Will show an error message below each field and will change the error field
     * to show red.
     * @constructor
     * @param  {jObject} object
     * @param  {Array} options
     * @param  {Function} callback - performs this function if successfully validated
     * @return valid8
     */
    var valid8 = function(object, options, callback) {
        var validatorID = guid();
        var hasHelpBlock = false;
        if (!object) {
            console.error("valid8: Could not load object.");
            return false;
        }
        if (options && options.helpBlock) {
            hasHelpBlock = true;
        }
        /**
         * Validates the form for email addresses, telphone numbers, required fields and etc.
         * @param {String} findElements - Comma-sparated string of element types to look for; ie: input, textarea
         * @return {bool}
         */
        this.validate = function(findElements){
            if(!findElements) {
                findElements = ":input";
            }
            var hasError = false;
            var errorlist = "The following must be completed before continuing\n";
            //Find all of the objects with input fields
            object.find($('div.valid8-error')).remove();
            object.find(findElements).filter("[required]").each(function(){
                var el = $(this);
                if(el.is('input, textarea')) { //we are dealing with an input
                    var type = el.attr('type'); //will either be 'text', 'radio', or 'checkbox
                    var objectHasError = false;

                    if (el.val().length === 0) {
                        el.after($("<div/>", {'class':'valid8-error valid8-error-message-field-' + validatorID}).css({fontSize:'small', paddingBottom: '5px', color:'red'}).text("Required field."));
                        objectHasError = true;
                        hasError = true;
                    } else {
                        var message = null;
                        var rs = null;
                        switch(type) {
                            case "email":
                                rs = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
                                if (!rs.test(el.val())) {message = "Invalid Email Address."; objectHasError = true;}
                            break;
                            case "tel":
                                rs = /[0-9 -()+]+$/;
                                if (!rs.test(el.val())) {message = "Invalid Phone Number."; objectHasError = true;}
                            break;
                        }
                        if (objectHasError){
                            el.after($("<div/>", {'class':'valid8-error valid8-error-message-field-' + validatorID}).css({fontSize:'small', paddingBottom: '5px', color:'red'}).text(message));
                            hasError = true;
                        }


                    }
                } else if(el.is('select')) { //we are dealing with a select
                    //code here
                } else { //we are dealing with a textarea
                    //code here
                }

            });

            if (options) {
                if (options.confirmPassword && options.password && options.passwordConfirm) {
                    if ((options.password.val() !== options.passwordConfirm.val())) {
                        if (hasHelpBlock) {
                            options.passwordConfirm.next(options.helpBlock).text("Passwords don't match.");
                        } else {
                            errorlist += "  - Passwords don't match.\n";
                        }
                        hasError = true;
                    }
                }
            }

            if (hasError) {
                //alert(errorlist);
                return false;
            } else {
                return true;
            }
        };


        this.clearForm = function(){
            object.find(':input').each(function(){
                $(this).val("");
            });
        };

        //Display an error in a helpBlock div chosen, loaded in the form
        this.showError = function(message,errorHelpBlockObj){
            if (!errorHelpBlockObj) {
                console.error("formValidation.showError: Object not set");
                return false;
            }
            errorHelpBlockObj.text(message);
            return true;
        };

        function guid() {
          function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
              .toString(16)
              .substring(1);
          }
          return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
            s4() + '-' + s4() + s4() + s4();
        }

    };
}
else
    console.error("valid8: Plugin already exists.");